#+filetags: remote-sensing otb research open-source free-culture free-software
#+DATE: <2014-11-26 Wed 09:00>
#+TITLE: Is open and free global land cover mapping possible?

Short answer: *yes*.  

Mid November took place in Toulouse "[[http://2014.capitoledulibre.org/][Le Capitole du libre]]", a
conference on Free Software and Free Culture. The program this year
was again full of interesting talks and workshops.

This year, I attended a [[http://2014.capitoledulibre.org/programme/presentation/50/][workshop]] about contributing to [[https://www.openstreetmap.org/][Openstreetmap]]
(OSM) using the [[http://josm.openstreetmap.de/][JOSM software]]. The workshop was organised by
[[https://www.dinot.net/][Sébastien Dinot]] who is [[http://www.openstreetmap.org/user/Sebastien%20Dinot/history#map=7/44.830/2.502][a massive contributor]] to OSM, and more
importantly a very nice and passionate fellow. 

I was very happy to learn to use JOSM and did [[http://www.openstreetmap.org/user/Jordi%20Inglada/history#map=15/43.5851/1.4792][2 minor contributions]]
right there.

During the workshop I learned that, over the past, OSM has been
enriched using massive imports from open data sources, like for
instance [[https://en.wikipedia.org/wiki/Cadastre][cadastral data bases]] from different countries or the 
[[http://wiki.openstreetmap.org/wiki/Corine_Land_Cover][Corine Land Cover]] data base. This has been possible thanks to the
policies of many countries which have understood that the [[https://en.wikipedia.org/wiki/Commons][commons]] are
important for the advancement of society. One example of this is the
European [[https://en.wikipedia.org/wiki/Infrastructure_for_Spatial_Information_in_the_European_Community][INSPIRE]] initiative.

I was also interested to learn that what could be considered niche
data, like agricultural land parcel data bases as for instance the French [[http://wiki.openstreetmap.org/wiki/WikiProject_France/data.gouv.fr/Occupation_du_sol][RPG]]
have also been imported into OSM. Since I have been using the RPG at
work for the last 4 years (see for example [[http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=6352606&queryText%3Dinglada+bayesian+networks][here]] or [[http://ieeexplore.ieee.org/xpl/articleDetails.jsp?tp=&arnumber=6352607&queryText%3Dinglada+segmentation+rpg][here]]), I was
sympathetic with the difficulties of OSM contributors to efficiently
exploit these data. I understood that the Corine Land Cover import was
also difficult and the results were not fully satisfactory.

As a matter of fact, roads, buildings and other cartographic objects
are easier to map than land cover, since they are discrete and
sparse. They can be pointed, defined and characterised more easily
than natural and semi-natural areas.

After that, I could not avoid making the link with what we do at work
in terms of preparing the exploitation of upcoming satellite missions
for automatic land cover map production.

One of our main interests is the use of [[http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-2][Sentinel-2]] images. It is the
week end while I am writing this, so I will not use my free time to
explain how land cover map production from multi-temporal satellite
images work: [[http://www.cesbio.ups-tlse.fr/multitemp/?p=1306][I already did it in my day job]].

What is therefore the link between what we do at work and OSM? The
revolutionary thing from my point of view is the fact that 
[[https://sentinel.esa.int/documents/247904/690755/TC_Sentinel_Data_31072014.pdf][Sentinel-2 data will be open and free]], which means that the OSM
project could use it to have a *constantly up to date land cover layer*.

Of course, Sentinel-2 data will come in huge volumes and a good amount
of expertise will be needed to use them. However, several public
agencies are paving the road in order to deliver data which is easy to
use. For instance, the [[http://www.theia-land.fr/en][THEIA Land Data Centre]] will provide Sentinel-2
data which is ready to use for mapping. The data will be available with
all the geometric and radiometric corrections of the best quality.

Actually, right now this is being done, for instance, for 
[[http://www.theia-land.fr/en/products/landsat-5-7-and-8][Landsat imagery]]. Of course, all these data is and will be available
under open and free licences, which means that anyone can start right
now learning how to use them.

However, going from images to land cover maps is not
straightforward. Again, a good deal of expertise and efficient tools
are needed in order to convert pixels into maps. This is what I have the
chance to do at work: *building tools to convert pixels into maps* which
are useful for real world applications. 

Applying the same philosophy to tools as for data, the tools we
produce are free and open. The core of all these tools is of course
the [[http://www.orfeo-toolbox.org][Orfeo Toolbox]], the Free Remote Sensing Image Processing Library
from [[http://www.cnes.fr][CNES]]. We have several times demonstrated that the tools are ready
to efficiently exploit satellite imagery to produce maps. For
instance, in [[http://www.cesbio.ups-tlse.fr/multitemp/?p=3461][this post here]] you even have the sequence of commands to
generate land cover maps using satellite image time series.

This means that we have free data and free tools. Therefore, the
complete pipeline is available for projects like OSM. OSM contributors
could start right now getting familiar with these data and these tools.

#+begin_quote
Head over to CNES servers to get some [[http://spirit.cnes.fr/resto/Landsat][Landsat data]], [[http://www.orfeo-toolbox.org/otb/download.html][install OTB]], get
familiar with [[http://www.cesbio.ups-tlse.fr/multitemp/?p=3461][the classification framework]] and see what could be done
for OSM.
#+end_quote

It is likely that some pieces may still be missing. For instance, the
main approach for the map production is *supervised classification*. This
means that we use machine learning algorithms to infer which land
cover class is present at every given site using the images as input
data. For these machine learning algorithms to work, we need training
data, that is, we need to know before hand the correct land cover
class in some places so the algorithm can be calibrated. 

This training data is usually called /ground truth/ and it is
expensive and difficult to get. In a global mapping context, this can
be a major drawback. However, there are interesting initiatives which
could be leveraged to help here. For instance, [[http://www.geo-wiki.org/][Geo-Wiki]] comes to mind
as a possible source of training data.

As always, talk is cheap, but it seems to me that exciting
opportunities are available for open and free quality global
mapping. This does not mean that the task is easy. It is not. There
are many issues to be solved yet and some of them are at the research
stage. But this should not stop motivated mappers and hackers to start
learning to use the data and the tools.
