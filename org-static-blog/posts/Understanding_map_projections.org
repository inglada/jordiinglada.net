#+title: Understanding map projections
#+date: <2011-02-09 Wed>
#+filetags: remote-sensing


Map projections are both easy and tricky. I am not a specialist at all
about the subject, but I have used them a little bit.When I say that map
projections are easy, I mean that, even without understanding them
completely, there are tools which allow a user to get the work one. Of
course, I use the [[http://www.orfeo-toolbox.org][Orfeo Toolbox]] library, which in turn uses the [[http://www.ossim.org][OSSIM]]
library.

Actually, some years ago, with a student, we designed the interface in
order to integrate OSSIM map projections (and sensor models too) into
OTB so that they could be used as the existing geometric transforms
already available in OTB (which came from [[http://www.itk.org][ITK]]).  The only thing we had
to understand in order to design this interface was that map projections
are coordinate transformations. Then, we chose the appropriate
object-oriented design pattern together with a bit of C++ templates for
the generic programming ingredient and that was it. When some users
started using the projections (well, *WE* were those users) questions
arised about the projections themselves. The task in answering to these
questions would have been much easier if we had had this info (posted by
Caitlin Dempsey on [[http://gislounge.com/][GIS Lounge]]):

#+begin_quote
The USGS has posted a scanned file of John P. Snyder's 1987 "Map
Projections: A Working Manual" online in PDF and DjVu format. The
beginning of the book contains substantial introductory information
about map projection families and distortions. Each projection is
started with a useful summary of the context and usage of that
particular projection. Snyder then delves into detail about the history,
features, and usage before providing the mathematical formulas used to
calculate the projection.
#+end_quote


