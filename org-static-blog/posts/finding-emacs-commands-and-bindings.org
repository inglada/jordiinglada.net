#+filetags: emacs tricks
#+DATE: <2013-07-17 Wed 18:32>
#+TITLE: Finding commands and key bindings in GNU/Emacs

Since I use GNU/Emacs for everything, I can't remember all the key
bindings for all the different modes (org-mode, gnus, geiser,
etc.). So I often use the command names using ~M-x command-name~.

Sometimes, I don't even remember the exact name of the command and I
will use the TAB completion to see suggestions. But for that, I need
to remember the beginning of the name of the command. If I remember a
part of the name of the command, say, /find/, typing ~M-x --find~
followed by TAB will propose all available commands whose name
contains /find/.

So far so good.

If I don't know at all the name of the command, there are a couple of
things that I can do. One would be browsing the menus in the menu
bar, but since my menu bar is hidden[fn:1] in order to increase
screen real estate, I would need to show it (~M-x menu-bar-mode~),
and grab the mouse. Instead, pressing ~F10~, a pop-up menu which can
be keyboard driven appears and I can browse the menus.

I only would do this if I was bored, but this is rarely the case, so
I prefer another possibility. I use ~C-h m~, which is bound to the
~describe-mode~ command which displays the documentation for all
active modes for the current buffer.

In this documentation there is a table showing all the available key
bindings and the corresponding commands. When in an org-mode buffer I
get this:

#+BEGIN_EXAMPLE
key             binding
---             -------

C-a             org-beginning-of-line
C-c             Prefix Command
C-e             org-end-of-line
TAB             org-cycle
C-j             org-return-indent
C-k             org-kill-line
RET             org-return
C-y             org-yank
ESC             Prefix Command
|               org-force-self-insert
...
#+END_EXAMPLE

Usually, this allows me to find the command I need and recall (at
least for 5 minutes) the corresponding key binding.

If I am not sure if a command does what I need, I can always use the
~describe-function~ (bound to ~C-h f~) command to get its
documentation. If I invoke the command with the cursor on one of the
commands of the table above, this will be used as default command to
be described by ~describe-function~.

Nice and efficient.




* Footnotes

[fn:1] I have ~(menu-bar-mode -1)~ in my ~.emacs~.

