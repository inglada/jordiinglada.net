#+title: svg2pdf
#+date: <2012-04-20 Fri>
#+filetags: note-to-self

When you need to convert an SVG file to PDF format (for inclusion into a
document, for instance) you can take several options:

- ImageMagick "convert" command will produce a raster PDF instead of a
  vector one. This may be enough for you and it is convenient, because
  it is command line based, and therefore scriptable

- Inkscape can load the SVG file and then you can export it as PDF. This
  will generate a vector PDF, which is great because it is scalable, but
  using a GUI-based solution can be fastidious if you have to convert a
  bunch o files.

Today I discovered that you can have both advantages (command line and
vector PDF) using Inkscape. Just use the  "--without-gui" option as
follows

#+BEGIN_SRC bash
  inkscape --without-gui --file=filename.svg --export-pdf=filename.pdf
#+END_SRC


