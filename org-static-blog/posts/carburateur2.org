#+filetags: fr books ideas philo eco life
#+DATE: <2015-05-06 Wed 09:00>
#+TITLE: Métiers fantomatiques et jugement infaillible de la réalité

J'ai l'impression que le livre de Crawford[fn:citeca] à propos duquel j'ai commencé à
écrire [[file:carburateur.org][ici]], va me donner beaucoup de matière pour ce blog.

Dans son ouvrage, Crawford "plaide plaide pour un idéal qui s'enracine
dans la nuit des temps mais ne trouve plus guère d'écho aujourd'hui :
le savoir-faire manuel et le rapport qu'il crée avec le monde matériel
et les objets de l'art." Il est conscient que son point de vue est
difficile à défendre dans notre /société de la connaissance/ :

#+BEGIN_QUOTE
Ce type de savoir-faire est désormais rarement convoqué dans
nos activités quotidiennes de travailleurs et de consommateurs, et
quiconque se risquerait à suggérer qu'il vaut la peine d'être cultivé
se verrait confronté aux sarcasmes du plus endurci des réalistes :
l'économiste professionnel. Ce dernier ne manquera pas, en effet, de
souligner les "coûts d'opportunité" de perdre son temps à fabriquer ce
qui peut être acheté dans le commerce. Pour sa part, l'enseignant
réaliste vous expliquera qu'il est irresponsable de préparer les
jeunes aux professions artisanales et manuelles, qui incarnent
désormais un stade révolu de l'activité économique. On peut toutefois
se demander si ces considérations sont aussi réalistes qu'elles le
prétendent, et si elles ne sont pas au contraire le produit d'une
certaine forme d'irréalisme qui oriente systématiquement les jeunes
vers les métiers les plus fantomatiques.
#+END_QUOTE

Par "métier fantomatique", Crawford entend les activités non directement
productives et dont le résultat n'est pas tangible ou mesurable.

#+BEGIN_QUOTE
Qu'est-ce qu'un "bon" travail, qu'est-ce qu'un travail susceptible de
nous apporter à la fois sécurité et dignité? Voilà bien une question
qui n'avait pas été aussi pertinente depuis bien
longtemps. Destination privilégiée des jeunes cerveaux ambitieux, Wall
Street, a perdu beaucoup de son lustre. Au milieu de cette grande
confusion des idéaux et du naufrage de bien des aspirations
professionnelles, peut-être verrons-nous réémerger la certitude
tranquille que le travail productif est le véritable fondement de
toute prospérité. Tout d'un coup, il semble qu'on n'accorde plus
autant de prestige à toutes ces méta-activités qui consistent à
spéculer sur l'excédent créé par le travail des autres, et qu'il
devient de nouveau possible de nourrir une idée aussi simple que : "Je
voudrais faire quelque chose d'utile".
#+END_QUOTE

Il est intéressant de noter qu'il appelle cela "un bon travail". Cette
qualification va au delà de l'aspect politique marxisant. Pour
Crawford, il est avant tout question de bien être de l'individu qui
exerce l'activité. Il explique, par exemple, sa propre expérience
d'électricien :

#+BEGIN_QUOTE
Le moment où, à la fin de mon travail, j'appuyais enfin sur
l'interrupteur ("Et la lumière fut") était pour moi une source
perpétuelle de satisfaction. J'avais là une preuve tangible de
l'efficacité de mon intervention et de ma compétence. Les conséquences
étaient visibles aux yeux de tous, et donc personne ne pouvait douter
de ladite compétence. Sa valeur sociale était indéniable.
#+END_QUOTE

Et la satisfaction ne venait pas seulement de cette "valeur sociale",
mais aussi (surtout?) la fierté du travail bien fait :

#+BEGIN_QUOTE
Ce qui ne m'empêchait pas de ressentir une certaine fierté
chaque fois que je satisfaisais aux exigences esthétiques d'une
installation bien faite. J'imaginais qu'un collègue électricien
contemplerait un jour mon travail. Et même si ce n'était pas le cas,
je ressentais une obligation envers moi-même. Ou plutôt, envers le
travail lui-même -- on dit parfois en effet que le savoir-faire
artisanal repose sur le sens du travail bien fait, sans aucune
considération annexe. Si ce type de satisfaction possède avant tout
un caractère intrinsèque et intime, il n'en reste pas moins que ce
qui se manifeste là, c'est une espèce de révélation,
d'auto-affirmation.  Comme l'écrit le philosophe Alexandre Kojève,
"l'homme qui travaille reconnaît dans le Monde effectivement
transformé par son travail sa propre œuvre : il s'y reconnaît
soi-même, il y voit sa propre réalité humaine, il y découvre et y
révèle aux autres la réalité objective de son humanité, de l'idée
d'abord abstraite et purement subjective qu'il se fait de lui-même."[fn:cak]
#+END_QUOTE

Et c'est le lien entre cette idée subjective de soi même et la réalité
objective du résultat produit qui fait qu'il y a des activités
professionnelles qui peuvent être très épanouissantes. Le revers de la
médaille est évidemment l'échec impossible à dissimuler, à différence
des métiers non productifs, dont les résultats sont difficiles à
mesurer quantitativement et même à évaluer qualitativement :

#+BEGIN_QUOTE
La vantardise est le propre de l'adolescent, qui est incapable
d'imprimer sa marque au monde. Mais l'homme de métier est soumis au
jugement infaillible de la réalité et ne peut pas noyer ses échecs ou
ses lacunes sous un flot d'interprétations. L'orgueil du travail bien
fait n'a pas grand-chose à voir avec la gratuité de l'"estime de soi"
que les profs souhaitent parfois instiller à leurs élèves, comme par
magie.
#+END_QUOTE

Crawford parle de "vantardise", ce qui peut-être compris comme étant
moqueur dans cette citation hors de contexte. De mon point de vue,
Crawford est très dur avec les activités qu'il appelle "fantomatiques"
plus haut, mais il ne s'attaque jamais aux individus qui les
exercent. Il y a dans le livre quelques pages sur les postes de
management[fn:manag] qui expliquent bien la difficulté d'occuper ce type de
poste :

#+BEGIN_QUOTE
Pour commencer, Jackall[fn:crj] observe que, malgré la nature essentiellement
bureaucratique du procès de travail moderne, les managers ne vivent
nullement leur rapport à l'autorité comme quelque chose
d'impersonnel. Cette autorité est en fait incarnée dans les personnes
concrètes avec lesquelles ils entrent en relation à tous les niveaux
de la hiérarchie. La carrière d'un individu dépend entièrement de ses
relations personnelles, entre autres parce que les critères
d'évaluation sont très ambigus. Par conséquent, les managers doivent
passer une bonne partie de leur temps à "gérer l'image que les autres
se font d'eux". Soumis sans répit à l'exigence de faire leurs preuves,
les managers vivent "dans une angoisse et une vulnérabilité
perpétuelles, avec une conscience aiguë de la probabilité constante de
bouleversements organisationnels susceptibles de faire capoter touts
leurs projets et d'être fatals à leur carrière", comme l'écrit Craig
Calhoun[fn:ccc] dans sa recension du livre de Jackall. Ils sont ainsi
systématiquement confrontés à la "perspective d'un désastre plus ou
moins arbitraire". 
#+END_QUOTE

Malheureusement, et par le [[https://fr.wikipedia.org/wiki/Principe_de_Peter][Principe de Peter]], les individus
techniquement compétents sont poussés à suivre des carrières qui les
amènent à des postes où ils finissent par perdre tout contact avec la
réalité concrète du travail et se retrouvent dans les [[file:expert.org][situations]]
décrites ci-dessus. Il reste encore des entreprises où l'excellence
technique est reconnue comme une filière aussi importante que le
management. 

D'un autre côté, il faut bien reconnaître que dans les
grosses structures il est nécessaire de gérer les équipes et que
quelqu'un doit assurer une vision d'ensemble des activités, même si ce
sont des tâches peu épanouissantes.


* Footnotes

[fn:citeca] Matthew B Crawford, Eloge du carburateur. Essai sur le sens et la valeur du travail, La Découverte, 2010, 249 p., EAN : 9782707160065.

[fn:cak] Alexandre Kojève Introduction à la lecture de Hegel, Gallimard, Paris, 1980, p.31-32

[fn:crj] R. Jackall, "Moral mazes : The world of corporate managers", Oxford University Press, 1988

[fn:ccc] C. Calhoun, "Why do bad careers happen to good managers?"

[fn:manag] Le management ici a un sens trè général et non pas
strictement hiérarchique. Par ailleurs, Crawford aborde la notion de
hiérarchie basée sur une compétence technique, qu'il juge assez positivement.


