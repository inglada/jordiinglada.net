(package-initialize)
(require 'org-static-blog)
(turn-on-font-lock)
(setq org-src-preserve-indentation t)
(setq org-src-fontify-natively t)
(org-babel-do-load-languages
 'org-babel-load-languages
 '((C . t) (python . t) (ipython . nil) (emacs-lisp . t) (latex . t) (shell . t) 
   (scheme . t) (gnuplot . t) (octave . t) (R . t)  (maxima . t)
   (calc . t)  (ditaa . t) (sqlite . t) (dot . t) (org . t)))
(setq org-static-blog-publish-title "Jordi Inglada")
(setq org-static-blog-publish-url "http://jordiinglada.net/sblog/")
(setq org-static-blog-publish-directory "~/Perso/WebPresence/jordiinglada.net/org-static-blog/")
(setq org-static-blog-posts-directory "~/Perso/WebPresence/jordiinglada.net/org-static-blog/posts/")
(setq org-static-blog-drafts-directory "~/Perso/WebPresence/jordiinglada.net/org-static-blog/drafts/")
(setq org-static-blog-enable-tags t)
(setq org-export-with-toc nil)
(setq org-export-with-section-numbers nil)
(setq org-static-blog-page-header
      "<meta name=\"author\" content=\"Jordi Inglada\">
<meta name=\"referrer\" content=\"no-referrer\">
<link href= \"static/tufte.css\" rel=\"stylesheet\" type=\"text/css\" />
<link rel=\"icon\" href=\"static/favicon.ico\"> <script type=\"text/javascript\" async   src=\"https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-MML-AM_CHTML\"> </script>
")
(setq org-static-blog-page-preamble
      "<div class=\"header\">
  <a href=\"http://jordiinglada.net/sblog/\">Jordi Inglada</a>
<div class=\"links\">
  <a href=\"http://jordiinglada.net/sblog/archive.html\">Archive</a> | <a href=\"http://jordiinglada.net/sblog/rss.xml\">RSS</a>
</div>
</div>")
(setq org-static-blog-page-postamble
      "<center><a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/4.0/88x31.png\" /></a><br /><span xmlns:dct=\"https://purl.org/dc/terms/\" href=\"https://purl.org/dc/dcmitype/Text\" property=\"dct:title\" rel=\"dct:type\">jordiinglada.net</span> by <a xmlns:cc=\"https://creativecommons.org/ns#\" href=\"https://www.jordiinglada.net\" property=\"cc:attributionName\" rel=\"cc:attributionURL\">Jordi Inglada</a> is licensed under a <a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/4.0/\">Creative Commons Attribution-ShareAlike 4.0 Unported License</a>. <a rel=\"license\" href=\"rss.xml\">RSS.</a> Feedback: info /at/ jordiinglada.net</center>")

;; https://emacs.stackexchange.com/questions/38437/org-mode-batch-export-missing-syntax-highlighting
(require 'font-lock)

(require 'subr-x) ;; for `when-let'

(unless (boundp 'maximal-integer)
  (defconst maximal-integer (lsh -1 -1)
    "Maximal integer value representable natively in emacs lisp."))

(defun face-spec-default (spec)
  "Get list containing at most the default entry of face SPEC.
Return nil if SPEC has no default entry."
  (let* ((first (car-safe spec))
     (display (car-safe first)))
    (when (eq display 'default)
      (list (car-safe spec)))))

(defun face-spec-min-color (display-atts)
  "Get min-color entry of DISPLAY-ATTS pair from face spec."
  (let* ((display (car-safe display-atts)))
    (or (car-safe (cdr (assoc 'min-colors display)))
    maximal-integer)))

(defun face-spec-highest-color (spec)
  "Search face SPEC for highest color.
That means the DISPLAY entry of SPEC
with class 'color and highest min-color value."
  (let ((color-list (cl-remove-if-not
             (lambda (display-atts)
               (when-let ((display (car-safe display-atts))
                  (class (and (listp display)
                          (assoc 'class display)))
                  (background (assoc 'background display)))
             (and (member 'light (cdr background))
                  (member 'color (cdr class)))))
             spec)))
    (cl-reduce (lambda (display-atts1 display-atts2)
         (if (> (face-spec-min-color display-atts1)
            (face-spec-min-color display-atts2))
             display-atts1
           display-atts2))
           (cdr color-list)
           :initial-value (car color-list))))

(defun face-spec-t (spec)
  "Search face SPEC for fall back."
  (cl-find-if (lambda (display-atts)
        (eq (car-safe display-atts) t))
          spec))

(defun my-face-attribute (face attribute &optional frame inherit)
  "Get FACE ATTRIBUTE from `face-user-default-spec' and not from `face-attribute'."
  (let* ((face-spec (face-user-default-spec face))
     (display-attr (or (face-spec-highest-color face-spec)
               (face-spec-t face-spec)))
     (attr (cdr display-attr))
     (val (or (plist-get attr attribute) (car-safe (cdr (assoc attribute attr))))))
    ;; (message "attribute: %S" attribute) ;; for debugging
    (when (and (null (eq attribute :inherit))
           (null val))
      (let ((inherited-face (my-face-attribute face :inherit)))
    (when (and inherited-face
           (null (eq inherited-face 'unspecified)))
      (setq val (my-face-attribute inherited-face attribute)))))
    ;; (message "face: %S attribute: %S display-attr: %S, val: %S" face attribute display-attr val) ;; for debugging
    (or val 'unspecified)))

(advice-add 'face-attribute :override #'my-face-attribute)

(progn
  (outline-show-all)
  (font-lock-flush)
  (font-lock-fontify-buffer)
  (org-static-blog-publish) )

